## OpenAI-based YouTube tool

This Python script:

- Downloads the audio from a YouTube video and saves it as an MP3 file.
- Renames the file, based on the video title, and hyphenates it.
- Transcribes the file through Whisper.
- If the audio is not in English, translates the transcript with Whisper.
- Creates a summary of the transcript, with an introductory paragraph and bullet points.

The code is based on a project from Andrei Dumitrescu's excellent [OpenAI with Python Bootcamp course](https://www.udemy.com/course/openai-api-chatgpt-gpt4-with-python-bootcamp/) on Udemy.

To use it, you need to add a `key.txt` file containing your OpenAI API key.
